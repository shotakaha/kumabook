## 0.2.0 (2022-10-13)

### Feat

- **docs/gilbert/ch01/index.md**: ギルバート本の1章の目次を追加した
- **docs/nicholas/ch03/index.md**: ニコラス本の第3章を追加した
- **docs/nicholas/ch02/problems.md**: ニコラス第2章の問題を追加しようとしている
- **docs/nicholas/ch02/sec22.md**: ニコラス第2.22節まで追加した
- **docs/nicholas/ch02/sec18.md**: ニコラス第2.18節まで追加した
- **docs/nicholas/ch02/sec15.md**: 第2.15節の一部を追加した
- **docs/nicholas/ch02/sec14.md**: 第2.14節を追加した

### Fix

- **docs/nicholas/ch02/sec10.md**: 2.1節から2.10節まで追記した
- **docs/conf.py**: 数式番号を参照するときの表示形式を設定した
- **docs/nicholas/ch02/sec10.md**: 平均を求める計算を追加した
- **docs/nicholas/ch02/sec5.md**: 内容を微修正した
- **renamed**: ニコラス第二章のファイル名を変更した

## 0.1.0 (2022-10-13)

### Feat

- **docs/conf.py**: physicsパッケージを追加した
- **docs/nicholas/ch02/index.md**: ニコラス第2章をいろいろ追加した
- **docs/nicholas/ch02/section7.md**: ニコラス第2.7節を追加した
- **docs/nicholas/ch02/section6.md**: ニコラス第2.6節を追加した
- **docs/nicholas/ch02/section10.md**: ニコラスの第2.10節を追加した
- **docs/nicholas/ch02/section5.md**: ニコラスの第2.5節を追加した
- **docs/nicholas/ch02/section8.md**: ニコラス第8章を追加した

### Fix

- **pyproject.toml**: versionを修正した
- **docs/conf.py**: LaTeXの見出しのトップレベルをpart（部）に変更した
- **docs/conf.py**: 数式オプションを追加した
- **docs/conf.py**: MySTの設定を追加した
- **docs/conf.py**: テーマの設定を変更した
- **docs/conf.py**: テーマの設定を追加した
- **docs/nicholas/ch02/section2.md**: 教科書の数式番号をラベルとして追加してみた
- **docs/nicholas/ch02/section9.md**: ポアソン分布を整理した
- **docs/nicholas/ch02/section8.md**: 二項分布を整理した
- **docs/nicholas/ch02/section5.md**: 実用的な平均値の計算を追記した
- **docs/nicholas/ch02/section5.md**: 平均値の汎用的な求め方を追記した
- **docs/nicholas/ch02/section3.md**: align環境は不要なので削除した
- **docs/index.md**: 目次にギルバート本を追加した
- **docs/nicholas/ch02/section4.md**: ニコラス第2.4節を追記した
- **nicholas/ch02/index.md**: ニコラス第二章を追加した
- **docs/nicholas/ch01.md**: ニコラス第1章を追加しようとしている
- **docs/gilbert/index.md**: ギルバート本を追加した
- **docs**: start sphinx docs
