# 3.2 Elements of Relativistic Kinematics （相対論の要素）

1905年にアインシュタインが提唱した特殊相対論（special theory of relativity）の柱は2つある；
「物理法則の共変性」と「光速度不変の原理」。

:物理法則の共変性:
    物理法則は慣性系によらない
:光速度不変の原理:
    真空中の光速は、光源の運動によらない

## 相対論による予言

1. 速度が変化すると質量も変化する
2. 質量とエネルギーは等価である（{math}`E=mc^{2}`）

アインシュタインの予言は、理論を発表してから数年後に実証された。
そして、今でも正しい。


## 相対論

```{math}
M^{*} = \frac{M}{\sqrt{1-\beta^{2}}} = \gamma M
```

```{math}
\beta & \equiv \frac{v}{c}\\
\gamma & \equiv \frac{1}{\sqrt{1-\beta^{2}}}
```