# 3.4 Nuclei（原子核）

:$A$: 質量数
:$N$: 中性子数
:$Z$: 陽子数 = 元素の原子番号

```{math}
M_{N}(A, Z) = ZM_{p} + NM_{n} - B(A, Z)c^{2}
```

:$Mp$: 陽子の質量
:$Mn$: 中性子の質量
:$B$: 原子核の束縛エネルギー