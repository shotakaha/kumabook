# 3.7 Energetics of Nuclear Decays （原子核の崩壊）

## 3.7.1 Gamma Decay（ガンマ崩壊）

```{math}
^{A}_{Z} X^{*} \rightarrow ^{A}_{Z} X + \gamma
```

エネルギー保存則と運動量保存則を考える。
原子はほぼ動かないので、原子の運動エネルギー（反跳エネルギー）は無視してよい。


### 内部転換電子

原子核の脱励起エネルギーは電子に与えられることがある。
この時の放出される電子を内部転換電子と呼ぶ。

## 3.7.2 Alpha Decay（アルファ崩壊）

アルファ線はヘリウムの原子核。

```{math}
^{A}_{Z} X \rightarrow ^{A-4}_{Z-2} X + ^{4}_{2}He
```

:$Q_{\alpha}$: 崩壊エネルギー

娘核（むすめかく／じょうかく）


## 3.7.3 Beta Decay（ベータ崩壊）

3体崩壊。電子と反ニュートリノを放出。

```{math}
^{A}_{Z} X \rightarrow ^{A}_{Z+1} X + \beta^{-} + \overline{\nu}
```

## Electron Capture （電子捕獲）

電子が原子核に捕獲されて、ニュートリノを放出する。