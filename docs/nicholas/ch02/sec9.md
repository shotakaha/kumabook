# 2.9 Poisson Distribution（ポアソン分布）

ポアソン分布は、確率が低く（{math}`p \to 0`）、試行回数が多い（{math}`N \to \infty`）実験に当てはめることができる。

## ポアソン分布の例

1. 原子核の崩壊数
2. 雷に打たれて殺される数
3. 配電盤で受信する電話の数
4. 励起された原子核から放出される光子の数
5. 宇宙線の数

などなど。

イベントの平均値が大きくなるにつれて、平均値を中心とした対称的な分布になる。
平均値が十分大きいと（{math}`m>20`？）、正規分布になる。

## 定義

````{admonition} ポアソン分布
```{math}
P(n) = \frac{m^{n}}{n!} e^{-m}
```
````

### 二項分布からポアソン分布を導出

二項分布は

```{math}
B(n, p) = \frac{N!}{n! (N-n)!} p^{n} (1-p)^{(N-n)}
```

ここで平均値{math}`Np = m`を一定にして、{math}`p \to 0`にする（と{math}`N \to \infty`になる）とポアソン分布になる。

```{math}
Np = m \Rightarrow p = \frac{m}{N}
```

```{math}
B(n, p)
&= \frac{N!}{n! (N-n)!} \left( \frac{m}{N} \right)^{n} \left(1- \frac{m}{N} \right)^{(N-n)}\\
&= \frac{N!}{n! (N-n)!}
\frac{m^{n}}{N^{n}}
\left(1- \frac{m}{N} \right)^{N}
\left(1- \frac{m}{N} \right)^{-n}\\
```

ここで左端のかたまりの一部を抜き出して計算すると

```{math}
\frac{N!}{(N-n)! N^{n}}
&= \frac{N (N-1) (N-2) \dots (N-n+1)}{N^{n}}\\
&= \frac{N}{N} \frac{N-1}{N} \frac{N-2}{N} \dots \frac{N-n+1}{N}\\
&= 1
\left( 1 - \frac{1}{N} \right)
\left( 1 - \frac{2}{N} \right)
\dots
\left( 1 - \frac{n-1}{N} \right)\\
(N \to \infty) \quad & \rightarrow 1
```

また、右端の項を{math}`N \to \infty`にして計算すると

```{math}
\left(1- \frac{m}{N} \right)^{-n} \rightarrow (1)^{-n} = 1
```

これを戻して

```{math}
B(n, p)
= \frac{m^{n}}{n!}
\left(1- \frac{m}{N} \right)^{N}
```

さらに{math}`-m/N \equiv h`と置いて計算すると

```{math}
-\frac{m}{N} \equiv h & \Rightarrow N = -\frac{m}{h}\\
\left( 1 - \frac{m}{N} \right)^{N}
& = (1 + h )^{-m/h}
= \left( (1 + h)^{1/h} \right)^{-m}\\
(N \to \infty \Rightarrow h \to 0) \quad & \rightarrow e^{-m}
```

````{tip}
ぜんぜん忘れてたけど、ネイピア数の定義の思い出し

```{math}
e \equiv \lim_{n \to \infty} \left( 1 + \frac{1}{n} \right)^{n}
= \lim_{h \to 0} \left( 1 + h \right)^{1/h}
```
````

これを戻して

```{math}
B(n, p) = \frac{m^{n}}{n!} e^{-m} \equiv P(n)
```

となり、ポアソン分布が得られる。

````{note}
教科書では平均値を{math}`m`を使っているが、
ポアソン分布の場合は{math}`\lambda`を使うことが多い。
確率変数も{math}`k`を使うことが多い。

```{math}
P(k) = \frac{\lambda^{k}}{k!} e^{-\lambda}
```
````

### 平均値

````{admonition} 平均値
```{math}
\overline{n} = m
```
````

```{math}
\overline{n}
&= \sum_{n=0}^{\infty} n P(n)\\
&= \sum_{n=0}^{\infty} n \frac{m^{n}}{n!} e^{-m}\\
&= \sum_{n=0}^{\infty} \frac{m^{n}}{(n-1)!} e^{-m}\\
&= m \sum_{n=0}^{\infty} \frac{m^{n-1}}{(n-1)!} e^{-m}\\
&= m \cdot 1\\
&= m\\
```

### 分散

````{admonition} 分散
```{math}
V(n) = m
```
````

```{math}
V(n)
= \overline{(n-m)^{2}}
&= \sum_{n=0}^{\infty} (n-m)^{2} P(n)\\
&= \sum_{n=0}^{\infty} (n^{2} - 2nm + m^{2}) P(n)\\
&= \sum_{n=0}^{\infty} n^{2} P(n) - 2m \sum_{n=0}^{\infty} n P(n) + m^{2} \sum_{n=0}^{\infty} P(n)\\
&= \sum_{n=0}^{\infty} n^{2} P(n) - 2m \cdot m + m^{2} \cdot 1\\
&= \sum_{n=0}^{\infty} n^{2} P(n) - 2m^{2} + m^{2}\\
&= \sum_{n=0}^{\infty} n^{2} P(n) - m^{2}\\
\qq{ここで}
\sum_{n=0}^{\infty} n^{2} P(n)
&= \sum_{n=0}^{\infty} (n(n-1) + n) P(n)\\
&= \sum_{n=0}^{\infty} n(n-1) P(n) + \sum_{n=0}^{\infty} n P(n)\\
&= \sum_{n=0}^{\infty} n(n-1) \frac{m^{n}}{n!} e^{-m} + m\\
&= m^{2} \sum_{n=0}^{\infty} \frac{m^{n-2}}{(n-2)!} e^{-m} + m\\
&= m^{2} + m\\
\qq{よって}
V(n)
&= m^{2} + m - m^{2}\\
&= m
```

### 標準偏差

````{admonition} 標準偏差
```{math}
\sigma = \sqrt{m}
```
````

```{math}
\sigma
&= \sqrt{V(n)}\\
&= \sqrt{m}
```

## 例題

### Example 2.10

```{admonition} 例題2.10
放射線源から放出される粒子を検出する。
検出率の平均が20cpm（count per minutes）と分かっている場合、次の測定で18cpmを得る確率を求めよ。
```

放射性元素の崩壊はポアソン分布に従う。

```{math}
P_{18} = \frac{20^{18}}{18!} e^{-20} = 0.0844
```

10000回測定した場合、結果が18cpmとなる測定は844回と推定できる。

### Example 2.11

```{admonition} 例題2.11
人口が比較的一定の都市を考える。
この都市の毎年の自動車による死亡者数は平均75人である。
次の都市に死亡者数が80人になる確率を求めよ。
```

```{math}
P_{80} = \frac{75^{80}}{80!} e^{-75} =  0.038
```