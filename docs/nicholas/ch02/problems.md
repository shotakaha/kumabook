# Problems

The energy distribution of thermal (slow) neutrons in a light-water reactor follows very closely the Maxwell–Boltzmann distribution:

```{math}
N(E) \dd E = A \sqrt{E} \exp{ \pqty{ - \frac{E}{kT} } } \dd E
```

where

```{math}
N(E) \dd E &= \qq{number of neutrons with kinetic energy between} E \textrm{と} E + \dd E\\
k &= \qq{Boltzmann constant} = 1.380662 \times10^{-23} J/°K\\
T &= \qq{temperature, K}\\
A &= \qq{constant}
```

Show that

- a. The mode of this distribution is {math}`E = (1/2)kT`.
- b. The mean is {math}`\overline{E} = (3/2) kT`.