# 2.15 Propagation of Errors （誤差の伝搬）

## 2.15.1 The Average and Its Standard Deviation for Quantities with More than One Random Variable

## 2.15.2 Examples of Error Propagation --- Uncorrelated Variables

### 誤差の伝搬式

足し算、引き算、掛け算、割り算、冪乗、指数などの場合の誤差の伝搬式を列挙する。

#### Example 2.13

{math}`f(x_{1}, x_{2}) = a_{1} x_{1} \pm a_{2} x_{2}`のとき

````{admonition} Example 2.13
```{math}
\overline{f} &= a_{1} \overline{x}_{1} \pm a_{2} \overline{x}_{2}\\
\sigma_{f} &= \sqrt{ a_{1}^{2} \sigma_{1}^{2} + a_{2}^{2} \sigma_{2}^{2} }
```
````

#### Example 2.14

{math}`f(x_{1}, x_{2}) = a x_{1} x_{2}`のとき

````{admonition} Example 2.14
```{math}
\overline{f} &= a \overline{x}_{1} \overline{x}_{2}\\
\sigma_{f} &= a \sqrt{ \overline{x}_{2}^{2} \sigma_{1}^{2} + \overline{x}_{1}^{2} \sigma_{2}^{2} }\\
\frac{ \sigma_{f} }{ \overline{f} } &= \sqrt{ \pqty{ \frac{ \sigma_{1} }{ \overline{x}_{1} } }^{2} + \pqty{ \frac{ \sigma_{2} }{ \overline{x}_{2} } }^{2} }
```
````

#### Example 2.15

{math}`f(x_{1}, x_{2}) = a x_{1} / x_{2}`のとき

````{admonition} Example 2.15
```{math}
\overline{f} &= a \frac{ \overline{x}_{1} }{ \overline{x}_{2} }\\
\sigma_{f} &= a \sqrt{ \frac{1}{\overline{x}_{2}^{2}} \sigma_{1}^{2} + \frac{\overline{x}_{1}^{2}}{\overline{x}_{2}^{4}} \sigma_{2}^{2} }\\
\frac{ \sigma_{f} }{ \overline{f} } &= \sqrt{ \pqty{ \frac{ \sigma_{1} }{ \overline{x}_{1} } }^{2} + \pqty{ \frac{ \sigma_{2} }{ \overline{x}_{2} } }^{2} }
```
````

#### Example 2.16

{math}`f(x) = x_{1}^{m}`のとき

````{admonition} Example 2.16
```{math}
\overline{f} &= \pqty{ \overline{x}_{1} }^{m}\\
\sigma_{f} &= m \pqty{ \overline{x}_{1} }^{m-1} \sigma_{1}\\
\frac{ \sigma_{f} }{ \overline{f} } &= m \frac{ \sigma_{1} }{ \overline{x}_{1} }
```
````

#### Example 2.17

{math}`f(x) = e^{ax_{1}}`のとき

````{admonition} Example 2.16
```{math}
\overline{f} &= e^{a \overline{x}_{1} }\\
\sigma_{f} &= e^{a \overline{x}_{1} } \sigma_{1}\\
\frac{ \sigma_{f} }{ \overline{f} } &= a \sigma_{1}
```
````