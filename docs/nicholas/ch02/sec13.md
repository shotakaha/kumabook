# 2.13 Arithmetic Mean and Its Standard Error （算術平均と標準誤差）

**真の値（true value）** は測定できないが、測定を繰り返すことで、測定誤差を小さくすることができる。

ここで、N回の測定をして、N個の測定結果{math}`n_{i} (i=1, 2, \dots, N)`が得られたとする。
測定結果{math}`n_{i}`が得られる確率を{math}`P(n_{i})`とする。

{math}`n_{i}`に対して{math}`P(n_{i})`をプロットしてヒストグラムを作成する。
Nが大きくなるにつれて、ガウス分布のような形になる。

## 算術平均（Arithmetic Mean）

````{admonition} 算術平均
```{math}
\overline{n} = \frac{n_{i} + n_{2} + \dots + n_{N}}{N} = \sum_{i=1}^{N} \frac{n_{i}}{N}
```
````

Nが大きくなるほど、測定誤差が小さくなり、nの真の平均値に近づく。
真の平均値は **無限回の測定** でしか得ることができない。
無限回の測定は不可能なので、現実には有限回の測定から平均値を求めるしかない。

## 標準誤差（Standard Error）

````{admonition} 不変分散
```{math}
\sigma^{2} = \sum_{i=1}^{N} \frac{(n_{i} - \overline{n})^{2}}{N-1}
```
````

真の平均値{math}`m`は（絶対に）分からないので、測定した平均値{math}`\overline{n}`で置き換えている。
また、分母を{math}`N-1`にしている。
測定回数Nが大きいと影響がないが、Nが少ないときにはこうしたほうがいい。

```{note}
素粒子実験ではNがひたすら大きいので、正直、気にしたことがなく、この違いはよく分かっていない。
```
