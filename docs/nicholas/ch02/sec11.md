# 2.11 Lorentzian Distribution （ローレンツ分布）

- 相互作用が粒子のエネルギーの関数で変化する現象に当てはまる
- 平均値{math}`m`を中心とした対称的な分布になる
- 原子核反応の共鳴状態はローレンツ分布になる

## 定義

```{math}
L(x) dx = \frac{1}{\pi} \frac{ \Gamma/2 }{ (x-m)^{2} + \Gamma^{2}/4 }
```

- {math}`L(x)dx`: {math}`x + dx`の範囲で確率変数{math}`x`がとりうる確率
- {math}`m`: 平均値

## 平均値

```{math}
\overline{x} = \int_{-\infty}^{\infty} x L(x) dx \equiv m
```

## 分散

ローレンツ分布の分散は計算できない。

## 標準偏差

分散が計算できないので、標準偏差も計算できない。
そのため、分布関数のFWHM（Full Width Half Maximum）で幅{math}`\Gamma`を定義する。
