# 2.10 Normal (Gaussian) Distribution（正規分布）

- 連続型確率分布
- 平均{math}`m`と分散{math}`\sigma^{2}`で定義される確率分布
- {math}`x=m`を中心とした対称的分布
- {math}`x=m`のときに最大値
- {math}`x`の範囲は{math}`(-\infty, +\infty)`

## 定義：正規分布の密度関数

```{math}
G(x) \dd x = \frac{1}{\sqrt{2\pi} \sigma} \exp{ \left( - \frac{(x-m)^{2}}{2\sigma^{2}} \right) } \dd x
```

:{math}`G(x) \dd x`:
    {math}`x`が起こる確率。微小区間{math}`[x, x + \dd x]`の面積
:{math}`m`:
    平均
:{math}`\sigma^{2}`:
    分散

````{note}
教科書は平均を{math}`m`と書いているが、ガウス分布の場合{math}`\mu`を使うことが多い。

```{math}
G(x; \mu, \sigma^{2}) \dd x = \frac{1}{\sqrt{2\pi} \sigma} \exp{ \left( - \frac{(x - \mu)^{2}}{2\sigma^{2}} \right) } \dd x
```
````

### 平均

````{admonition} 平均
```{math}
\bar{x} = \int_{-\infty}^{\infty} x G(x) dx
```
````

```{math}
\overline{x}
&= \int_{-\infty}^{\infty} x G(x) \dd x\\
(x-m \rightarrow y)
&= \int_{-\infty}^{\infty} (y+m) G(y+m) \dd y\\
&= \int_{-\infty}^{\infty} y G(y+m) \dd y
+ m \int_{-\infty}^{\infty} G(y+m) \dd y\\
&= 0 + m \cdot 1\\
&= m
```

最初に{math}`y=x-m`と置き換えて計算を開始する。
この置き換えで{math}`x=y+m`、{math}`\dd x = \dd y`となるので、それぞれ代入する。
最後の方では、
右辺の第1項は「奇関数と偶関数の掛け算なので、積分するとゼロ」、
右辺の第2項は「ガウス分布を全区間で積分すると1」
になることを使って計算している。

### 分散

````{admonition} 分散
```{math}
V(x) = \int_{-\infty}^{\infty} (x-m)^{2} G(x) dx
```
````

## 累積正規分布関数（cumulative normal distribution function）

```{math}
E(X) = \int_{-\infty}^{x} G(x') \dd x'
```

### 1シグマ（68.3%）

```{math}
A_{\sigma} = \int_{m-\sigma}^{m+\sigma} G(X) \dd x = 0.683
```

- {math}`1 \sigma` : 68%
- {math}`2 \sigma` : 95%
- {math}`3 \sigma` : 99%

## 半値幅（FWHM; Full Width Half Maximum）

正規分布の最大値の半分の場所での分布の幅{math}`\Gamma`を半値幅と呼ぶ。

## 標準正規分布

{math}`m = 0`と{math}`\sigma = 1`の正規分布を **標準正規分布（standard normal distribution）** と呼ぶ。

````{admonition} 標準化
```{math}
t = \frac{x-m}{\sigma}
```
````

## 中心極限定理

元の測定結果の分布が正規分布に従わない場合でも、その測定の平均値の平均値は正規分布に近づく。


## ポアソン分布？正規分布？

平均値{math}`m > 20`のポアソン分布は正規分布として扱ってよい。