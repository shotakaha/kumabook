# 2.6 Dispersion Indexes, Variance, and Standard Deviation （分散指数（分散と標準偏差））

平均値の周りにどれくらい確率分布密度が広がっているかも重要。

## 分散

````{admonition} 離散型確率分布
```{math}
V(x) = \sum_{i=1}^{N} (x_{i} - m)^{2} f(x_{i})
```
````

````{admonition} 連続型確率分布
```{math}
V(x) = \int_{-\infty}^{\infty} (x - m)^{2} f(x) dx
```
````

## 標準偏差

````{admonition} 標準偏差
```{math}
\sigma = \sqrt{V(x)}
```
````


### 線形結合の分散

````{admonition} 線型結合の分散
```{math}
V(a + bx) = b^{2}V(x)
```
````

```{math}
V(a+bx) &= \int_{-\infty}^{\infty} \pqty{ (a+bx) - \overline{(a+bx)} }^{2} f(x) \dd x\\
&= \int_{-\infty}^{\infty} \pqty{ (a+bx) - (a+b \overline{x}) }^{2} f(x) \dd x\\
&= \int_{-\infty}^{\infty} \pqty{ b (x - \overline{x}) }^{2} f(x) \dd x\\
&= b^{2} \int_{-\infty}^{\infty} \pqty{ x - \overline{x} }^{2} f(x) \dd x\\
&= b^{2} V(x)
```