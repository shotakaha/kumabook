# 2.5 Location Indexes (Mode, Median, Mean) （位置指数（最頻値、中央値、平均値））

確率分布や累積分布関数が分かっていると、確率変数{math}`x`に関するさまざまな情報が得られる。

## Mode（最頻値）

```{math}
\dv{f(x)}{x} = 0 \qq{となる} x
```

## Median（中央値）

```{math}
\int_{-\infty}^{x} f(x) \dd x = \frac{1}{2} \qq{となる} x
```

## Mean（平均値）

````{admonition} 離散型確率分布
```{math}
\overline{x} = \sum_{i=1}^{N} x_{i} f(x_{i})
```
````

````{admonition} 連続型確率分布
```{math}
\overline{x} = \int_{-\infty}^{\infty} x f(x) dx
```
````

## （汎用的な）平均値

任意の関数{math}`g(x)`の平均値は次のように計算できる。

````{admonition} 離散型確率分布
```{math}
\overline{g(x)} = \frac{\sum_{i=1}^{N} g(x_{i}) f(x_{i})}{\sum_{i=1}^{N} f(x_{i})}
```
````

````{admonition} 連続型確率分布
```{math}
\overline{g(x)} = \frac{\int_{-\infty}^{\infty} g(x) f(x) dx}{\int_{-\infty}^{\infty} f(x) dx}
```
````


## 平均値の性質

```{math}
\overline{ax} &= a \overline{x}\\
\overline{a + x} &= a + \overline{x}\\
\overline{g_{1}(x) + g_{2}(x) + \dots + g_{i}(x)} & = \overline{g_{1}(x)} + \overline{g_{2}(x)} + \dots + \overline{g_{i}(x)}
```

## 算術平均（arithmetic mean of series of N random variables）

{math}`N`回の測定を行い、測定結果{math}`x_{i}`（=N個の測定値）を得た場合の平均値は、次のように計算できる。
実験して得られた値の平均値は、これで計算する。

```{math}
\overline{x} = \frac{1}{N} \sum_{i=1}^{N} x_{i}
```

### すべての測定結果の平均値

同じ内容の測定を、複数回に分けて行う場合
（たとえば、測定時間の制限だったり、データ容量の制限だったり）がある。
それぞれの測定で得られた{math}`M`個の平均値{math}`\overline{x_{j}}`を使って、全体の平均値を計算できる。

```{math}
\overline{X} = \frac{1}{M} \sum_{j=1}^{M} \overline{x_{j}}
```

## 例題

### Example2.5

````{admonition} 例題2.5
放射性原子核が時刻{math}`t`で崩壊しない確率は、{math}`\lambda`を定数として、次のようになる。

```{math}
f(t) = \lambda e^{-\lambda t}
```

この原子核の平均寿命（mean life）を求めよ。
````

```{math}
\overline{t}
&= \int_{0}^{\infty} t f(t) dt\\
&= \int_{0}^{\infty} t \lambda e^{-\lambda t} dt\\
&= \lambda \int_{0}^{\infty} t e^{-\lambda t} dt\\
&= \mathrm{（あとで確認する）}\\
&= \frac{1}{\lambda}
```

### Example2.6

````{admonition} 例題2.6
サイコロを振ることを考える。
1から6の目が出る確率が1/6とする。
出る目の平均値を求めよ。
````

{math}`x_{i} = 1, 2, 3, 4, 5, 6`、{math}`f(x) = 1/6`の、
離散型確率分布の平均値を求める。

```{math}
\overline{x}
& = \sum_{i=1}^{N} x_{i} f(x_{i})\\
& = \sum_{n=1}^{6} n \cdot \frac{1}{6}\\
& = \frac{1}{6} \cdot \frac{6 (6+1)}{2}\\
& = \frac{1}{6} \cdot 21\\
& = \frac{7}{2} = 3.5
```