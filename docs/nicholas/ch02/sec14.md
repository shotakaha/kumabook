# 2.14 Confidence Limits （信頼度）

多くの標本から取り出した$i$番目のサンプルの値を$x_{i}$とする。
たとえば、球の直径だったり、燃料棒の容器の厚みだったり、燃料棒の長さだったり、なんでもよい。
実際にものづくりをすると、設計図で指定した値と比べて、少しずつバラツキが生じる。

サンプルを測定した平均値を **公称値（nominal value）** としてバラツキ（**公差**）を調べることができる。
この公差が大きすぎては困るので、どれくらいまでOKするか、を決めるのが **信頼度**。
ほとんどの場合、正規分布を仮定して信頼度を算出／設定する。

```{math}
G(x) &= \frac{1}{\sqrt{2 \pi} \sigma} \exp{ \pqty{ - \frac{(x - x_{n})^{2}}{ 2 \sigma^{2}} } }\\
\qq{where}
x_{n} & = \qq{nominal value of x} = \qq{avarage value of x}\\
\sigma &= \qq{standard deviation of the distribution}
```

許容値（acceptable value）を{math}`x_{a}`としたとき、その許容値より大きくなってしまう確率は

```{math}
P(x > x_{a}) = \int_{x_{a}}^{\infty} \frac{1}{\sqrt{2f\pi} \sigma} \exp{ \pqty{ - \frac{ (x-x_{n})^{2} }{ 2 \sigma^{2}} } } \dd x
```

となる。
また、許容値は標準偏差{math}`\sigma`を使って

```{math}
x_{a} = x_{n} + k\sigma
```

と表すことができる。
いわゆる「何シグマ」というもの。
教科書（p.43）のTABLE2.2にあるように、{math}`k`の値が大きくなるほど、許容値より大きくなる確率は小さくなり、信頼度は高くなる。

## 標準正規分布

（平均{math}`\mu = 0`、分散{math}`\sigma^{2} = 1`の正規分布）

```{math}
G(x) &= \frac{1}{\sqrt{2 \pi} } \exp{ \pqty{ - \frac{ x^{2} }{ 2 } } }\\
P(x > x_{a}) &= \int_{x_{a}}^{\infty} \frac{1}{\sqrt{2\pi} } \exp{ \pqty{ - \frac{ x^{2} }{ 2 } } } \dd x
```