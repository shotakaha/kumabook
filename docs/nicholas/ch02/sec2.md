# 2.2 Definition of Probability（確率の定義）

測定を{math}`N`回行い、{math}`x`という結果が{math}`n`回得られた場合の確率{math}`P(x)`は {eq}`eq2.2`（Equation2.2）のように表すことができる。

```{math}
---
label: eq2.2
---
P(x) = \frac{n}{N}
```

実験回数を大きくすればするほど（{math}`N \to \infty`）、理論値（真の値？）に近づく（Eq2.1）。

```{math}
---
label: eq2.1
---
P(x) = \lim_{N \to \infty} \frac{n}{N}
```