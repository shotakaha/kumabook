# 2.18 Standard Error of Counting Rates （計数率の標準誤差）

放射線測定の場合、線源がある状態とない状態で、放出された放射線の数を測定する。
線源がある状態での測定結果が{math}`G`（gross count）、
ない状態での測定結果が{math}`B`（background）だったとする。
また、それぞれの測定時間は{math}`t_{G}`と{math}`t_B`だったとする。

```{math}
\qq{gross count rate} g &= \frac{G}{t_{G}}\\
\qq{background count rate} b &= \frac{B}{t_{B}}\\
\qq{net counting rate} r &= g - b = \frac{G}{t_{G}} - \frac{B}{t_{B}}
```

計数率{math}`r`の標準誤差は{eq}`eq2.84`（教科書p.54 Eq.2.94）を使って計算できる。
{math}`r`を4つ変数（{math}`G, t_{G}, B, t_{B}`）の関数と考えて、

```{math}
\sigma_{r} =
\sqrt{
\pqty{ \pdv{r}{G} }^{2} \sigma_{G}^{2}
+ \pqty{ \pdv{r}{t_{G}} }^{2} \sigma_{t_{G}}^{2}
+ \pqty{ \pdv{r}{B} }^{2} \sigma_{B}^{2}
+ \pqty{ \pdv{r}{t_{B}} }^{2} \sigma_{t_{B}}^{2}
}
```

最近の計測機器は時間の不定性が小さいので{math}`\sigma_{t_{G}}, \sigma_{t_{B}}`は、ここでは無視して考える。
ただし、時間の精度も必要な測定はこの限りではない。

```{math}
\sigma_{r} =
\sqrt{
\pqty{ \pdv{r}{G} }^{2} \sigma_{G}^{2}
+ \pqty{ \pdv{r}{B} }^{2} \sigma_{B}^{2}
}
```

必要な要素を計算する。

```{math}
\pdv{r}{G} &= \frac{1}{t_{G}}\\
\pdv{r}{B} &= \frac{1}{t_{B}}
```

GとBの標準誤差は

```{math}
\sigma_{G} &= \sqrt{G}\\
\sigma_{B} &= \sqrt{B}
```

よって、計数率のエラー{math}`\sigma_{r}`は

```{math}
\sigma_{r}
= \sqrt{
\frac{G}{t_{G}^{2}}
+ \frac{B}{t_{B}^{2}}
}
```

となり、測定値{math}`G, B, t_{G}, t_{B}`から計算できる。
計数率{math}`g, b`ではないことは重要なポイント。