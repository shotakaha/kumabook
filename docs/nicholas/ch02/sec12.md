# 2.12 Standard, Probable, and Other Errors

```{math}
R & \pm E \quad \textrm{(absolute error)}

R & \pm \epsilon \% \quad \textrm{(relative error)}
```

{math}`R \pm E`とは、正しい値が{math}`R-E`と{math}`R+E`の間に存在するということではなく、**正しい結果がこの範囲にありそう** という意味。
どれくらいの確率で正しそうなのか？と言われると、かっちりした決まりはなく、分野によって使いかがたことなる場合もある。
ただし、これまでの研究から **標準誤差（standard error）** と **確率誤差（probable error）** の2種類の誤差の考え方が確立している。
両方の誤差の考え方は正規分布（ガウス分布）をベースにしている。

## 標準誤差（standard error）

{math}`R \pm E_{s}` の範囲に正しい結果（true result）の存在する確率が68.3%となる誤差{math}`E_{s}`を標準誤差と呼ぶ。
正規分布で{math}`R-E_{s}`から{math}`R+E_{s}`の範囲の面積が0.683になるときの{math}`E_{s}`というとイメージしやすいと思う。

```{math}
\int_{R-E_{s}}^{R+E_{s}} G(x; R, \sigma) = 0.683
```

## 確率誤差（probable error）

{math}`R \pm E_{p}` の範囲に正しい結果（true result）の存在する確率が50%となる誤差{math}`E_{p}`を確率誤差と呼ぶ。
正規分布で{math}`R-E_{p}`から{math}`R+E_{p}`の範囲の面積が0.5になるときの{math}`E_{p}`というとイメージしやすいと思う。

```{math}
\int_{R-E_{p}}^{R+E_{p}} G(x; R, \sigma) = 0.5
```

## 標準誤差と確率誤差の関係

```{math}
E_{p} = 0.6745 E_{s}
```

## その他の誤差の付け方

正規分布を仮定して何%で誤差を定義するかは人（や分野）それぞれ。
論文でそれを読み取ることが大事。