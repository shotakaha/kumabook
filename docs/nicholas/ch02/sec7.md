# 2.7 Covariance and Correlation （共分散と相関）

- {math}`X_{1}, X_{2}, \dots, X_{M}` : 確率変数
- {math}`m_{1}, m_{2}, \dots, m_{M}` : 平均値
- {math}`\sigma^{2}_{1}, \sigma^{2}_{2}, \dots, \sigma^{2}_{M}` : 分散

```{math}
Q = a_{1} X_{1} + a_{2} X_{2} + \dots + a_{M} X_{M} = \sum_{i=1}^{M} a_{i} X_{i}
```

## 平均値

````{admonition} 平均値
```{math}
\overline{Q} = \sum_{i=1}^{M} a_{i} m_{i}
```
````

```{math}
\overline{Q}
& = \overline{a_{1} X_{1} + a_{2} X_{2} + \dots + a_{M} X_{M}}\\
& = a_{1} \overline{X_{1}} + a_{2} \overline{X_{2}} + \dots + a_{M} \overline{X_{M}}\\
& = a_{1} m_{1} + a_{2} m_{2} + \dots + a_{M} m_{M}\\
& = \sum_{i=1}^{M} a_{i} m_{i}
```

## 分散

````{admonition} 分散
```{math}
V(Q) =
\sum_{i=1}^{M} a_{i}^{2} \sigma_{i}^{2}
+ 2 \sum_{j>i}^{M} \sum_{i}^{M} a_{i} a_{j} \overline{ (X_{i} - m_{i}) (X_{j} - m_{j})}
```
````

````{admonition} 共分散
```{math}
\textrm{cov}(X_{i}, X_{j})= \overline{ (X_{i} - m_{i}) (X_{j} - m_{j})}
```
````

```{math}
V(Q)
& = \overline{(Q - \overline{Q})^{2}}\\
```

## 相関係数

共分散は単位に依存している。
標準偏差で割ることで無次元量にして、使いやすくしたのが相関係数（correlation coefficient）

```{math}
\rho
```