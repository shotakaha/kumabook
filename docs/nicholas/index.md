# ニコラス本

:Title:
    Measurement and Detection of Radiation (4th Edition)
:Author:
    - Nicholas Tsoulfanidis
    - Sheldon Landsberger
:Amazon:
    https://www.amazon.co.jp/dp/B00UVBSEJC

```{toctree}
---
maxdepth: 2
---
ch01/index
ch02/index
ch03/index
```

---

## TBA

- Chapter4. Energy Loss and Penetration of Radiation through Matter
- Chapter5. Gas-Filled Detectors
- Chapter6. Scintillation Detectors
- Chapter7. Semiconductor Detectors
- Chapter8. Relative and Absolute Meaturements
- Chapter9. Introduction of Spectroscopy
- Chapter10. Electronics for Radiation Counting
- Chapter11. Data Analysis Methods
- Chapter12. Photon (γ-ray and X-ray) Spectroscopy
- Chapter13. Charged-Particle Spectroscopy
- Chapter14. Neutron Detection and Spectroscopy
- Chapter15. Activation Analysis and Related Techniques
- Chapter16. Health Physics Fundamentals
- Chapter17. Nuclear Forensics
- Chapter18. Nuclear Medicine Instrumentation
- Appendix A: Useful Constants and Conversion Factors
- Appendix B: Atomic Masses and Other Properties of Isotopes
- Appendix C: Alpha, Beta, and Gamma Sources Commonly Used
- Appendix D: Tables of Photon Attenuation Coefficients
- Appendix E: Table of Buildup Factor Constants