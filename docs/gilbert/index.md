# ギルバート本

:Title:
  Communication and Engagement with Science and Technology - Issues and Dilemmas (1st Edition)
:Authors:
  - John K. Gilbert
  - Susan M. Stocklmayer
:Amazon:
  https://www.amazon.co.jp/dp/B00AHANQM2



- Part1. Models of Science Communication - Theory Into Practice
  - Chapter1. Towards a "Design Approach" to Science Communication
  - Chapter2. Engagement with Science: Models of Science Communication
- Part2. Challenges in Communicating Science
  - Chapter3. Scientists' Engagement with the Public
  - Chapter4. The Role of Science and Technology in Public Policy. What is Knowledge for ?
  - Chapter5. Negotiating Public Resistance to Engagement in Science and Technology
- Part3. Major Themes in Science Communication
  - Chapter6. Communicating the Significance of Risk
  - Chapter7. Quantitative Literacy in Science Communication
  - Chapter8. Ethics and Accountability in Science and Technology
  - Chapter9. Beliefs and the Value of Evidence
- Part4. Informal Learning
  - Chapter10. Helping Learning in Science Communication
  - Chapter11. Science Communication and Science Education
  - Chapter12. The Practice of Science and Technology Communication in Science Museums
- Part5. Communication of Contemporary Issues in Science and Society
  - Chapter13. Communicating Global Climate Change Issues and Dilemmas
  - Chapter14. Science Communication During a Short-Term Crisis: The Case of Severe Acute Respiratory Syndrome (SARS)
  - Chapter15. Communication Challenges for Sustainability
  - Chapter16. The Value of Indigenous Knowledge Systems in the 21st Century
  - Chapter17. Science Communication: The Consequences of Being Human
- Part6. Further Exploration
