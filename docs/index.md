% KumaBook documentation master file,
% created by sphinx-quickstart on Thu Oct 6 23:05:52 2022.
% You can adapt this file completely to your liking,
% but it should at least contain the root `toctree` directive.

# KumaBook

```{toctree}
---
maxdepth: 2
---
nicholas/index
thomson/index
gilbert/index
```

## Indices and tables

- {ref}`genindex`
- {ref}`modindex`
- {ref}`search`
