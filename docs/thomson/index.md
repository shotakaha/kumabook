# Modern Paticle Physics / Mark Thomson

- Amazon : https://www.amazon.co.jp/dp/B00DOW6NMA

---

- Chapter1. Introduction
- Chapter2. Underlying concepts
- Chapter3. Decay rates and cross sections
- Chapter4. The Dirac equation
- Chapter5. Interaction by particle exchange
- Chapter6. Electron-positron annihilation
- Chapter7. Electron-proton elastic scattering
- Chapter8. Deep inelastic scattering
- Chapter9. Symmetries and the quark model
- Chapter10. Quantum Chromodynamics (QCD)
- Chapter11. The weak interaction
- Chapter12. The weak intaractions of leptons
- Chapter13. Neutrinos and neutrino oscillations
- Chapter14. CP violation and weak hadronic interactions
- Chapter15. Electroweak unification
- Chapter16. Tests of the Standard Model
- Chapter17. The Higgs Boson
- Chapter18. The Standard Model and beyond
- Appendix A: The Dirac delta-function
- Appendix B: Dirac equation
- Appendix C: The low-mass hadrons
- Appendix D: Gauge boson polarization states
- Appendix E: Noether's theorum
- Appendix F: Non-Abelian gauge theories
